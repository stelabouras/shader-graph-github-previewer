var shaderGraphPreviewer = {
	filename: undefined,
	shaderGraphRaw: undefined,
	didChange: false
};

function addPreviewIfNeededFromGist() {
	let table = document.querySelector('#gist-pjax-container table.js-file-line-container');

	if (table == undefined)
		return;

	var filename = table.dataset.tagsearchPath;

	if (filename == undefined) {
		let filenameElement = document.querySelector('a strong.gist-blob-name');

		if (filenameElement == undefined)
			return;

		filename = filenameElement.innerText;
	}

	if (filename == undefined || filename.length == 0)
		return;

	let fileExtension = filename.split('.').pop().toLowerCase();

	if (fileExtension != 'shadergraph' && fileExtension != 'shadersubgraph')
		return;

	var shaderGraphRaw = '';
	table.querySelectorAll('td.blob-code-inner').forEach((element) => {
		shaderGraphRaw += element.innerText;
	});

	if (shaderGraphRaw.length == 0)
		return;

	let rawContentButtons = document.querySelectorAll('a.Button--secondary.Button--small.Button > span');

	if (rawContentButtons.length == 0) {
		console.warn('Oh noes! Could not find a raw button to attach the button to!');
		return;
	}

	let rawContentButton = rawContentButtons[rawContentButtons.length - 1];
	addPreview(filename, shaderGraphRaw, rawContentButton, false);
}

function addPreviewIfNeeded() {
	let textarea = document.querySelector('#read-only-cursor-text-area')

	if (textarea == undefined) {
		addPreviewIfNeededFromGist();
		return;
	}

	var filenameElement = document.querySelector('#file-name-id');

	if (filenameElement == undefined)
		filenameElement = document.querySelector('#file-name-id-wide');

	if (filenameElement == undefined)
		return;

	let filename = filenameElement.innerText;
	let fileExtension = filename.split('.').pop().toLowerCase();

	if (fileExtension != 'shadergraph' && fileExtension != 'shadersubgraph')
		return;

	let shaderGraphRaw = textarea.value;

	let rawContentButton = document.querySelector('button[aria-label="Download raw content"]');

	if (rawContentButton == undefined) {
		console.warn('Oh noes! Could not find a download raw content button to attach the button to!');
		return;
	}

	addPreview(filename, shaderGraphRaw, rawContentButton, true);
}

function signalShaderGraphPreviewer(iframe) {
	shaderGraphPreviewer.didChange = false;
	// Base64 of the shadergraph raw
	// Convert the string to bytes, in order to work around
	// issue where characters can take up more than one byte
	// e.g. emojis, special characters like the apostrophe etc
	// Ref: https://developer.mozilla.org/en-US/docs/Glossary/Base64#the_unicode_problem
	let encodedBytes = new TextEncoder().encode(shaderGraphPreviewer.shaderGraphRaw);
	const binString = Array.from(encodedBytes, (byte) => String.fromCodePoint(byte)).join("");
	const base64Data = btoa(binString);
	const blob = new Blob([base64Data], {
		type: "application/json",
	});
	iframe.contentWindow.postMessage([blob, shaderGraphPreviewer.filename], "*");
};

function addPreview(filename, shaderGraphRaw, rawContentButton, createTooltipAsParent) {
	if (!shaderGraphPreviewer.didChange) {
		shaderGraphPreviewer.didChange = (filename != shaderGraphPreviewer.filename || shaderGraphRaw != shaderGraphPreviewer.shaderGraphRaw);
	}
	shaderGraphPreviewer.filename = filename;
	shaderGraphPreviewer.shaderGraphRaw = shaderGraphRaw;

	// If there is already a shadergraph previewer button attached to
	// DOM, just bail.
	if (document.getElementById("shadergraph-previewer-button") != undefined) {
		console.warn('Shadergraph previewer already found in DOM');
		return;
	}

	let rawContentTooltip = rawContentButton.parentElement;
	let rawContentTooltipClassName = rawContentTooltip.className;
	let rawContentContainer = rawContentTooltip.parentElement;

	var previewerTooltip;

	if (createTooltipAsParent) {
		previewerTooltip = document.createElement('span');
		previewerTooltip.id = "shadergraph-previewer-tooltip";
		previewerTooltip.className = rawContentTooltipClassName;
		previewerTooltip.setAttribute('aria-label', "Preview Shader Graph");
		previewerTooltip.setAttribute('role', 'tooltip');
	}
	else {
		previewerTooltip = document.createElement('tool-tip');
		previewerTooltip.id = "shadergraph-previewer-tooltip";
		previewerTooltip.setAttribute('for', 'shadergraph-previewer-button');
		previewerTooltip.setAttribute('popover', 'manual');
		previewerTooltip.setAttribute('data-direction', 's');
		previewerTooltip.setAttribute('data-type', 'label');
		previewerTooltip.setAttribute('data-view-component', 'true');
		previewerTooltip.className = 'sr-only position-absolute';
		previewerTooltip.innerText = 'Preview Shader Graph';
	}

	let previewer = document.createElement('button');
	previewer.id = "shadergraph-previewer-button";
	previewer.innerHTML = '<svg aria-hidden="true" focusable="false" role="img" class="octicon octicon-eye" viewBox="0 0 16 16" width="16" height="16" fill="currentColor" style="display: inline-block; user-select: none; vertical-align: text-bottom; overflow: visible;"><path d="M8 2c1.981 0 3.671.992 4.933 2.078 1.27 1.091 2.187 2.345 2.637 3.023a1.62 1.62 0 0 1 0 1.798c-.45.678-1.367 1.932-2.637 3.023C11.67 13.008 9.981 14 8 14c-1.981 0-3.671-.992-4.933-2.078C1.797 10.83.88 9.576.43 8.898a1.62 1.62 0 0 1 0-1.798c.45-.677 1.367-1.931 2.637-3.022C4.33 2.992 6.019 2 8 2ZM1.679 7.932a.12.12 0 0 0 0 .136c.411.622 1.241 1.75 2.366 2.717C5.176 11.758 6.527 12.5 8 12.5c1.473 0 2.825-.742 3.955-1.715 1.124-.967 1.954-2.096 2.366-2.717a.12.12 0 0 0 0-.136c-.412-.621-1.242-1.75-2.366-2.717C10.824 4.242 9.473 3.5 8 3.5c-1.473 0-2.825.742-3.955 1.715-1.124.967-1.954 2.096-2.366 2.717ZM8 10a2 2 0 1 1-.001-3.999A2 2 0 0 1 8 10Z"></path></svg>';
	previewer.dataset['component'] = 'IconButton';
	previewer.dataset['size'] = 'small';
	previewer.dataset['noVisuals'] = 'true';
	previewer.type = 'button';
	previewer.setAttribute('aria-label', "Preview Shader Graph");
	previewer.addEventListener('click', () => {
		if (document.getElementById("shadergraph-previewer-container") != undefined) {
			document.getElementById("shadergraph-previewer-container").style.display = 'block';
			document.body.dataset['shadergraphpreviewer'] = "1";
			if (shaderGraphPreviewer.didChange) {
				document.querySelector('#shadergraph-previewer-header span strong').innerText = shaderGraphPreviewer.filename;
				signalShaderGraphPreviewer(document.querySelector('#shadergraph-previewer'));
			}
			return;
		}
		let iframeContainer = document.createElement('div');
		iframeContainer.id = "shadergraph-previewer-container";

		let iframeHeader = document.createElement('div');
		iframeHeader.id = "shadergraph-previewer-header";
		iframeHeader.innerHTML = "<span><strong>" + filename + "</strong> | Preview by <a href='https://shadergraph.stelabouras.com/' title='Unity Shader Graph viewer' target='_blank'>Unity Shader Graph viewer</a>";

		let iframeHeaderCloseButton = document.createElement('button');
		iframeHeaderCloseButton.innerHTML = '<svg aria-hidden="true" focusable="false" role="img" class="octicon octicon-x" viewBox="0 0 16 16" width="16" height="16" fill="currentColor" style="display: inline-block; user-select: none; vertical-align: text-bottom; overflow: visible;"><path d="M3.72 3.72a.75.75 0 0 1 1.06 0L8 6.94l3.22-3.22a.749.749 0 0 1 1.275.326.749.749 0 0 1-.215.734L9.06 8l3.22 3.22a.749.749 0 0 1-.326 1.275.749.749 0 0 1-.734-.215L8 9.06l-3.22 3.22a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042L6.94 8 3.72 4.78a.75.75 0 0 1 0-1.06Z"></path></svg>';
		iframeHeaderCloseButton.dataset['component'] = 'IconButton';
		iframeHeaderCloseButton.dataset['hotkey'] = 'Escape';
		iframeHeaderCloseButton.dataset['noVisuals'] = 'true';
		iframeHeaderCloseButton.type = 'button';
		iframeHeaderCloseButton.addEventListener('click', () => {
			const iframeContainer = document.getElementById("shadergraph-previewer-container");
			if (iframeContainer != undefined)
				iframeContainer.style.display = 'none';
			document.body.dataset['shadergraphpreviewer'] = "0";
		});
		iframeHeader.appendChild(iframeHeaderCloseButton);

		iframeContainer.appendChild(iframeHeader);

		let loader = document.createElement('div');
		loader.id = "shadergraph-previewer-loader";
		loader.innerHTML = "Loading&hellip;";
		iframeContainer.appendChild(loader);

		// Create the iframe
		let iframe = document.createElement('iframe');
		iframe.setAttribute('allowtransparency', 'true');
		iframe.id = "shadergraph-previewer";
		iframe.frameBorder = 0
		iframe.src = "https://shadergraph.stelabouras.com/";
		// Wait for the iframe to load
		iframe.onload = function() {
			signalShaderGraphPreviewer(iframe);
		}
		iframeContainer.appendChild(iframe);
		document.body.appendChild(iframeContainer);

		document.body.dataset['shadergraphpreviewer'] = "1";
	});

	if (createTooltipAsParent) {
		previewerTooltip.appendChild(previewer);
	}
	else {
		rawContentContainer.appendChild(previewer);
	}

	rawContentContainer.appendChild(previewerTooltip);
}

document.addEventListener('keydown', (event) => {
	const iframeContainer = document.getElementById("shadergraph-previewer-container");
	if (iframeContainer == undefined)
		return;
	if (event.keyCode != 27)
		return;
	iframeContainer.style.display = 'none';
	document.body.dataset['shadergraphpreviewer'] = "0";
});

window.addEventListener("message", (event) => {
	if (event.origin != "https://shadergraph.stelabouras.com")
		return;
	if (event.data != "loaded")
		return;
	const loader = document.getElementById("shadergraph-previewer-loader");
	if (loader != undefined)
		loader.remove();
});

// Detect React DOM updates
const observer = new MutationObserver((mutations) => { 
	var mutationDetected = false;
	mutations.forEach((mutation) => { 
		if (mutationDetected)
			return;
		if (mutation.type != 'childList')
			return;
		if (mutation.target == document.body
			|| mutation.target == document.querySelector('div[data-target="react-app.reactRoot"]')
			|| mutation.target == document.querySelector('#repos-header-breadcrumb ol')
			|| mutation.target == document.querySelector('#repos-header-breadcrumb--wide')
			|| mutation.previousSibling == document.querySelector('#repos-header-breadcrumb--wide'))
			mutationDetected = true;
	});
	if (!mutationDetected)
		return;
	addPreviewIfNeeded();
});

observer.observe(document.body, {
	attributes: true,
	childList: true,
	subtree: true
});

// Detect DOM updates
if (document.readyState === "complete" 
	|| document.readyState === "loaded"
	|| document.readyState === "interactive") {
	addPreviewIfNeeded();
}
else {
	document.addEventListener('DOMContentLoaded', () => {
		addPreviewIfNeeded();
	});
}