# Shader Graph GitHub previewer

A simple Firefox add-on (I tested it and it also works on Google Chrome!) that adds a preview button on `.shadergraph` and `.shadersubgraph` files on GitHub repositories.

The preview is powered by the [Unity Shader Graph viewer](https://shadergraph.stelabouras.com/) online tool.

The tool has been built from scratch using Vanilla JS.

The icons are provided by [Octicons](https://primer.style/foundations/icons).

### Instructions when packing the extension for submission.

To ensure that no hidden files are included from the OS, you can do a `unzip -l Archive.zip`.

To remove the `.DS_Store` files: `zip -d Archive.zip "*DS_Store*"`.

To remove the `__MACOSX` directory: `zip -d Archive.zip "__MACOSX*"`.

### License

Shader Graph GitHub previewer is licensed under the [GNU License](https://gitlab.com/stelabouras/shader-graph-github-previewer/-/blob/main/LICENSE).
